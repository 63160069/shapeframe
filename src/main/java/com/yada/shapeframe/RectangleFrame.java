/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.shapeframe;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author ASUS
 */
public class RectangleFrame extends JFrame {
    JLabel lblWidth;
    JLabel lblHeight;
    JTextField txtWidth;
    JTextField txtHeight;
    JButton btnCalculate;
    JLabel lblResult;
    public RectangleFrame() {
        super("Rectangle");
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lblWidth = new JLabel("width:", JLabel.TRAILING);
        lblWidth.setSize(50, 20);
        lblWidth.setLocation(5, 5);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        this.add(lblWidth);
       
        txtWidth= new JTextField();
        txtWidth.setSize(50, 20);
        txtWidth.setLocation(60, 5);
        this.add(txtWidth);
       
        lblHeight = new JLabel("height:", JLabel.TRAILING);
        lblHeight.setSize(50, 20);
        lblHeight.setLocation(120, 5);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        this.add(lblHeight);
        
        txtHeight= new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(175, 5);
        this.add(txtHeight);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(150, 25);
        this.add(btnCalculate);

        lblResult = new JLabel("Rectangle width = ???, height =??? Area = ???"+ " Perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidth = txtWidth.getText();
                    double width = Double.parseDouble(strWidth);
                    String strHeight = txtHeight.getText();
                    double height = Double.parseDouble(strHeight);
                    Rectangle rectangle = new Rectangle(width,height);
                    lblResult.setText("Rectangle width = " + String.format("%.2f", rectangle.getWidth())
                            + ", height = "+String.format("%.2f", rectangle.getHeight())
                            + " Area = " + String.format("%.2f", rectangle.calArea())
                            + " Perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                } catch (Exception ex) {
                     JOptionPane.showMessageDialog(RectangleFrame.this, "Error: Please input number",
                             "Error",JOptionPane.ERROR_MESSAGE);
                     txtWidth.setText("");
                     txtWidth.requestFocus();
                     txtHeight.setText("");
                     txtHeight.requestFocus();
                }
            }
            
        });

        
    }
    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }
}
